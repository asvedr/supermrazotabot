import re
from mrazota.logger import logger

var_re = re.compile(r'\$[a-z]+')

def render_text(text, botself, variables):
    logger.debug('render_text: %s' % text)
    found = var_re.findall(text)
    if len(found) == 0:
        logger.debug('render_text: plain')
        return text
    logger.debug('render_text: found %s' % found)
    found = set(found)
    for var in found:
        value = variables[var](botself)
        logger.debug('render_text: var"%s" = "%s"' % (var, value))
        text = re.sub(r'\$%s\b' % var[1:], value, text)
    return text
