#!/usr/bin/env python
# -*- coding: utf-8 -*-

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from mrazota.logger import logger
import traceback


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Пизда')


def help(bot, update):
    """Send a message when the command /help is issued."""
    update.message.reply_text('Пуп')


def echo(mashone):
    def callback(bot, update):
        """Echo the user message."""
        text = update.message.text
        user = update.message.from_user
        userid = str(user['id'])
        username = user['first_name']
        has_photo = update.message.photo != []
        text += ' '
        if has_photo:
            text += '<$PHOTO>'
        try:
            answer = mashone.read_message(userid, username, text)
        except Exception as e:
            trace = traceback.format_exc()
            answer = str(trace)
            logger.error('exception on answe for %s' % text)
            logger.error(answer)
        if isinstance(answer, str):
            update.message.reply_text(answer)
        elif isinstance(answer, list):
            for line in answer:
                update.message.reply_text(line)
        elif answer is None:
            logger.debug('Empty answer for %s' % text)
        else:
            logger.debug('Strange answer for %s' % text)
            logger.debug('Answer: %s : %s' % (answer, type(answer)))
    return callback


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.debug('Update "%s" caused error "%s"', update, error)


def main(token, mashone):
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(token)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, echo(mashone)))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()
