#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import random
import enum
import datetime
import mrazota.utils as utils
from mrazota.logger import logger
from mrazota.text_renderer import render_text
import json

random.seed()
TIMEDELTA = datetime.timedelta(hours=0)

class TriggerType(enum.IntEnum):
    ''' This class is a trigger for messages
        When bot see trigger in some message in chat, bot start reaction
    '''
    # Return answer
    SIMPLE = 0
    # Return answer with replacing some word from original message
    REPLACER = 1 
    # No answer but change karma
    SILENT = 2
    # Contol Command to bot
    COMMAND = 3

class Trigger(object):

    __slots__ = ('regex',
                 'callback',
                 'answers',
                 'replacers',
                 'trigger_type',
                 'k_modificator',
                 'source',
                 'karma_predicate',
                 'refusal')

    @staticmethod
    def default_karma_predicate(_):
        ''' define it as static function for economy '''
        return True

    def __init__(self, regex, karma_predicate):
        # source phrase for debuging
        self.source = regex if type(regex) == str else '<N/A>'
        # regex for search
        self.regex = re.compile(regex, flags=re.IGNORECASE) if type(regex) == str else regex
        # callback for COMMAND
        self.callback = None
        # answers for SIMPLE
        self.answers = []
        # answers for REPLACER
        self.replacers = []
        # karma modificator
        self.k_modificator = 0
        # reaction can be started inly if karma predicator return True for user karma
        if karma_predicate is None:
            self.karma_predicate = self.default_karma_predicate
        else:
            self.karma_predicate = karma_predicate
        # if user has bad karma and 'refusal' not empty user will get something from refusal list
        self.refusal = []

    def __lt__(self, other):
        # need this for sort in bot start
        # sort used for setting priority
        return self.trigger_type < other.trigger_type

    @classmethod
    def Simple(cls, regex, answers, k_modificator=0, k_predicate=None):
        ''' 'answers' is list of (karma(int), string) '''
        trigger = cls(regex, k_predicate)
        trigger.answers = answers
        trigger.k_modificator = k_modificator
        trigger.trigger_type = TriggerType.SIMPLE
        return trigger

    @classmethod
    def Replacer(cls, regex, replace_to, k_predicate=None):
        ''' 'replace_to' is list of string '''
        trigger = cls(regex, k_predicate)
        trigger.replacers = replace_to
        trigger.trigger_type = TriggerType.REPLACER
        return trigger

    @classmethod
    def Command(cls, regex, callback, k_predicate=None, refusal=None):
        ''' callback is fun like (bot, trigger, userid, username, text) -> str '''
        trigger = cls(regex, k_predicate)
        trigger.callback = callback
        trigger.trigger_type = TriggerType.COMMAND
        if refusal is not None:
            trigger.refusal = refusal
        return trigger

    @classmethod
    def Silent(cls, regex, k_modificator):
        trigger = cls(regex, None)
        trigger.k_modificator = k_modificator
        trigger.trigger_type = TriggerType.SILENT
        return trigger


class Mashone(object):

    __slots__ = ('_triggers', '_alphabet', 'user_karma',
                 '_reaction', 'users', 'ids', 'render_vars',
                 '_active_user', 'REPLACE_TEMPLATE',
                 'WEEKDAYS', 'DELTA_KARMA', 'NEAREST_ANSWERS',
                 'MIN_KARMA', 'MAX_KARMA', 'karma_file_path')

    def __init__(self, alphabet, triggers, karma_file_path=None):
        ''' 'alphabet' is str with all letters for user language alphabet
            'triggers' is list of Trigger '''

        # Replace template can be replaced after creating
        self.REPLACE_TEMPLATE = 'Not {0} but {1}'
        self.WEEKDAYS = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
        self.DELTA_KARMA = 5
        self.NEAREST_ANSWERS = 3
        self.MIN_KARMA = -10
        self.MAX_KARMA = 10

        self.karma_file_path = karma_file_path
        # For correct ordering of trigger type
        triggers.sort()
        # Alphabet used for dividing phrase to words
        self.set_alphabet(alphabet)
        self._triggers = triggers
        # {id: karma}
        karma = {}
        try:
            if karma_file_path is not None:
                with open(karma_file_path, 'rt') as handler:
                    karma = json.loads(handler.read())
        except Exception:
            logger.debug('parse karma file error. use { }')
            karma = {}
        self.user_karma = karma
        # {id: name}
        self.users = {}
        # {name: id}
        self.ids = {}
        # set this var before call render
        self._active_user = None
        # We don't use self.attr in '_reaction' dict because of GC loop problem
        cls = self.__class__
        # callbacks for 
        self._reaction = {
            TriggerType.SIMPLE: cls.t_simple_answer,
            TriggerType.REPLACER: cls.t_replace_and_answer,
            TriggerType.SILENT: cls.t_modify_karma,
            TriggerType.COMMAND: cls.t_use_callback
        }
        # If in answer text there are variables they will be resolved using this dict
        # render get value from dict and call it as func
        self.render_vars = {
            '$username': cls.var_username,
            '$randomuser': cls.var_randomuser,
            '$time': cls.var_time,
            '$dayofweek': cls.var_dayofweek
        }

    def var_username(self):
        ''' variable '''
        return self._active_user

    def var_randomuser(self):
        ''' variable '''
        if len(self.users) == 0:
            return self._active_user
        else:
            return random.choice(tuple(self.users.values()))

    def var_time(self):
        ''' variable '''
        now = datetime.datetime.now() + TIMEDELTA
        return '%s:%s' % (now.hour, now.minute)

    def var_dayofweek(self):
        now = datetime.datetime.now() + TIMEDELTA
        return self.WEEKDAYS[now.weekday()]

    def set_alphabet(self, text):
        ''' turn text to set of char codes and save to private var '''
        self._alphabet = {ord(sym) for sym in text}

    def get_karma(self, id):
        ''' get user karma or 0 if not exist '''
        return self.user_karma.get(id, 0)

    def set_karma(self, id, val):
        ''' save user karma '''
        self.user_karma[id] = min(max(val, self.MIN_KARMA), self.MAX_KARMA)
        if self.karma_file_path is not None:
            with open(self.karma_file_path, 'wt') as handler:
                handler.write(json.dumps(self.user_karma))

    def read_message(self, userid, username, text):
        ''' read message and react if some trigger used '''

        logger.debug('accept message {}:{}'.format(userid, text))

        if not (userid in self.users):
            self.users[userid] = username
        if not (username in self.ids and self.ids[username] == userid):
            self.ids[username] = userid

        for trigger in self._triggers:
            if trigger.regex.search(text):
                logger.debug('trigger found "{}":{}'.format(trigger.source, trigger.trigger_type))
                if trigger.karma_predicate(self.get_karma(userid)):
                    callback = self._reaction[trigger.trigger_type]
                    answer = callback(self, trigger, userid, username, text)
                    return self.prepare_answer(username, answer)
                elif len(trigger.refusal) > 0:
                    answer = random.choice(trigger.refusal)
                    return self.prepare_answer(username, answer)

    def prepare_answer(self, username, answer):
        if answer is None:
            return None
        self._active_user = username
        if type(answer) == list:
            return [render_text(line, self, self.render_vars) for line in answer]
        else:
            return render_text(answer, self, self.render_vars)

    def t_modify_karma(self, trigger, userid, username, text):
        ''' callback for karma modifications '''
        modificator = trigger.k_modificator
        self.set_karma(userid, self.get_karma(userid) + modificator)

    def t_use_callback(self, trigger, userid, username, text):
        ''' callback for using callaback from trigger '''
        return trigger.callback(self, userid, username, text)

    def t_simple_answer(self, trigger, userid, username, text):
        ''' callback for simple answer '''
        self.t_modify_karma(trigger, userid, username, text)
        karma = self.get_karma(userid)
        def answer_good_for_karma(pair):
            if pair[0] is None:
                return True
            else:
                return abs(pair[0] - karma) <= self.DELTA_KARMA
        answers = list(filter(answer_good_for_karma, trigger.answers))
        if len(answers) == 0:
            answers = utils.get_closest(trigger.answers, karma, self.NEAREST_ANSWERS)
        return random.choice(answers)[1]

    def t_replace_and_answer(self, trigger, userid, username, text):
        ''' callback for replace answer '''
        for word in self._split_to_words(text):
            if trigger.regex.search(word):
                variant = random.choice(trigger.replacers)
                return self.REPLACE_TEMPLATE.format(word,
                                                    trigger.regex.sub(variant, word))
        raise Exception('t_replace_and_answer', 'can not replace')

    def _split_to_words(self, text):
        ''' split text to words (used in REPLACER) '''
        word = ''
        alphabet = self._alphabet
        for sym in text:
            if not (ord(sym) in alphabet):
                if len(word) > 0:
                    yield word
                word = ''
            else:
                word += sym
        if len(word) > 0:
            yield word
