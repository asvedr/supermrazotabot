def get_closest(pairs, karma, N):
    ''' pairs: (karma, answer) 
        karma: user karma
        N: how many answers get '''
    pairs = pairs[:]
    result = []
    for _ in range(N):
        min_i = None
        min_karma = None
        for i in range(len(pairs)):
            if pairs[i][0] is None:
                min_i = i
                break
            current_karma = abs(pairs[i][0] - karma)
            if min_karma is None or current_karma < min_karma:
                min_karma = current_karma
                min_i = i
        if min_i is None:
            break
        result.append(pairs[min_i])
        del pairs[i]
    return result