import logging

def _init_logger():
    logger = logging.getLogger('mrazota')
    handler = logging.FileHandler('mrazota.log')
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger

logger = _init_logger()