from mrazota.logic import *
from mrazota.text_renderer import *
import unittest

alphabet_low = ''.join([chr(c) for c in range(ord('a'), ord('z'))])
alphabet = alphabet_low + alphabet_low.upper()

class TestLogic(unittest.TestCase):

    curses = [
        (0, 'You are MRAZ'),
        (0, 'Hate you'),
        (0, 'Kill yourself')
    ]

    def test_simple(self):

        def show_best(mashone, userid, username, text):
            id = max(mashone.user_karma.items(), key=lambda a: a[1])[0]
            return mashone.users[id]

        triggers = [
            Trigger.Simple(r'dead.*(girl)|(w(oo)|(ee)man)', self.curses, -2),
            Trigger.Silent(r'\b[Hh]i\b', 1),
            Trigger.Replacer(r'boy', ['girl']),
            Trigger.Silent(r'shit', -1),
            Trigger.Command(r'Mashone.*who is the best', show_best),
            Trigger.Simple(r'multi', [(None, ['$username', 'b', 'c'])])
        ]
        mashone = Mashone(alphabet, triggers)
        ans = mashone.read_message(0, 'Ars', 'Ha ha, dead beauty wooman. Let\'s warm it')
        self.assertTrue(ans in map(lambda a: a[1], self.curses))
        ans = mashone.read_message(1, 'Ser', 'How about some dead dog?')
        self.assertTrue(ans is None)
        ans = mashone.read_message(2, 'Sas', 'Hi all. How is it going?')
        self.assertTrue(ans is None)
        ans = mashone.read_message(3, 'Asy', 'I see cowboy! Wow')
        self.assertEqual(ans, 'Not cowboy but cowgirl')
        ans = mashone.read_message(4, 'Vir', 'I laught at some shit')
        self.assertTrue(ans is None)
        ans = mashone.read_message(0, 'Ars', 'Mashone, tell me who is the best')
        self.assertEqual(ans, 'Sas')
        self.assertEqual(mashone.get_karma(0), -2)
        self.assertEqual(mashone.get_karma(1), 0)
        self.assertEqual(mashone.get_karma(2), 1)
        self.assertEqual(mashone.get_karma(3), 0)
        self.assertEqual(mashone.get_karma(4), -1)
        ans = mashone.read_message(0, 'Ars', 'multi')
        self.assertEqual(ans, ['Ars', 'b', 'c'])

    def test_order(self):
        triggers = [
            Trigger.Silent(r'Simp\+Sil-', -1),
            Trigger.Simple(r'Simp\+Sil-', [(0, 'a')], 1),
            Trigger.Simple(r'Simp-Sil\+', [(0, 'a')], -1),
            Trigger.Silent(r'Simp-Sil\+', 1)
        ]
        mashone = Mashone(alphabet, triggers)
        mashone.read_message(0, 'A', 'Simp+Sil-')
        mashone.read_message(1, 'B', 'Simp-Sil+')
        self.assertEqual(mashone.get_karma(0), 1)
        self.assertEqual(mashone.get_karma(1), -1)

    def test_access(self):

        def call_me(mashone, userid, username, text):
            return username

        triggers = [
            Trigger.Silent(r'Good', 1),
            Trigger.Silent(r'Bad', -1),
            Trigger.Command(r'Call me', call_me, k_predicate=lambda a: a > 0, refusal=['No']),
            Trigger.Simple(r'Suck', [(0, 'Fuck you $username')], k_predicate=lambda a: a < 0)
        ]
        mashone = Mashone(alphabet, triggers)
        self.assertEqual(mashone.read_message(0, 'Ars', 'Good'), None)
        self.assertEqual(mashone.read_message(1, 'Vir', 'Bad'), None)
        self.assertEqual(mashone.read_message(0, 'Ars', 'Call me'), 'Ars')
        self.assertEqual(mashone.read_message(0, 'Ars', 'Suck'), None)
        self.assertEqual(mashone.read_message(1, 'Vir', 'Call me'), 'No')
        self.assertEqual(mashone.read_message(1, 'Vir', 'Suck'), 'Fuck you Vir')

    def test_karma_in_simple(self):

        good = [
            (0, 'zero'),
            (3, 'three'),
            (5, 'five')
        ]

        bad = [
            (0, 'down'),
            (-3, 'very'),
            (-5, 'out')
        ]

        triggers = [
            Trigger.Simple(r'Good', good),
            Trigger.Simple(r'Bad', bad),
            Trigger.Silent(r'\+', 1),
            Trigger.Silent(r'-', -1)
        ]
        mashone = Mashone(alphabet, triggers)
        mashone.NEAREST_ANSWERS = 1
        mashone.DELTA_KARMA = 0

        self.assertEqual(mashone.read_message(0, 'Ars', 'Good'), 'zero')
        self.assertEqual(mashone.read_message(1, 'Ser', 'Bad'), 'down')

        for i in range(3):
            mashone.read_message(0, 'Ars', '+')
            mashone.read_message(1, 'Ser', '-')

        self.assertEqual(mashone.read_message(0, 'Ars', 'Good'), 'three')
        self.assertEqual(mashone.read_message(1, 'Ser', 'Bad'), 'very')
        self.assertEqual(mashone.read_message(0, 'Ars', 'Bad'), 'down')
        self.assertEqual(mashone.read_message(1, 'Ser', 'Good'), 'zero')

        for i in range(4):
            mashone.read_message(0, 'Ars', '+')
            mashone.read_message(1, 'Ser', '-')

        self.assertEqual(mashone.read_message(0, 'Ars', 'Good'), 'five')
        self.assertEqual(mashone.read_message(1, 'Ser', 'Bad'), 'out')
        self.assertEqual(mashone.read_message(0, 'Ars', 'Bad'), 'down')
        self.assertEqual(mashone.read_message(1, 'Ser', 'Good'), 'zero')

    def test_renderer(self):
        mashone = Mashone('', [])
        vars = mashone.render_vars.copy()
        vars['$a'] = lambda _: '123'
        self.assertEqual(render_text('hello $a?', mashone, vars), 'hello 123?')
