import mrazota.utils as utils
import unittest

class TestUtils(unittest.TestCase):

    def test_closest(self):
        source = [(0,'a'), (3,'b'), (5,'c')]
        a = utils.get_closest(source, 0, 1)[0][1]
        b = utils.get_closest(source, 3, 1)[0][1]
        c = utils.get_closest(source, 5, 1)[0][1]
        self.assertEqual((a,b,c), ('a', 'b', 'c'))

    def test_closest_none(self):
        source = [(0,'a'), (3,'b'), (5,'c'), (None, 'd')]
        a = utils.get_closest(source, 0, 2)
        b = utils.get_closest(source, 5, 2)
        a = ''.join(map(lambda x: x[1], a))
        b = ''.join(map(lambda x: x[1], b))
        self.assertTrue(a == 'ad' or a == 'da')
        self.assertTrue(b == 'cd' or b == 'dc')
