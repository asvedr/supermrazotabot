import masha_setup.phrases as masha
import unittest
import mrazota.logic as logic
from mrazota.text_renderer import render_text

class TestPhrases(unittest.TestCase):

    def test_valid(self):
        attrs = filter(lambda s: s.startswith('speak_'), dir(masha))
        bot = logic.Mashone('', [])
        def mock_fun(bot):
            return ''
        mock_vars = {key: mock_fun for key in bot.render_vars.keys()}
        for attr in attrs:
            phrases = getattr(masha, attr)
            for phrase in phrases:
                phrase = phrase[1]
                self.assertTrue(type(phrase) == str or type(phrase) == list)
                if type(phrase) == str:
                    lines = [phrase]
                else:
                    lines = phrase
                for line in phrase:
                    render_text(line, bot, mock_vars)
