from mrazota.logic import Trigger
from masha_setup import phrases

def gte(count):
    return lambda k: k >= count

def lte(count):
    return lambda k: k <= count

def userupkarma(mashone, userid, username, text):
    usernames = mashone.users.values()
    userids = mashone.users.keys()
    for word in filter(''.__ne__, text.split(' ')):
        if word == userid or word == username:
            continue
        if word in usernames:
            id = mashone.ids[word]
            k = mashone.get_karma(id)
            mashone.set_karma(id, max(k, 0))
        elif word in userids:
            k = mashone.get_karma(word)
            mashone.set_karma(word, max(k, 0))
    return

def debug_raise(mashone, userid, username, text):
    raise Exception('Debug exception for %s' % userid)

def show_best(mashone, userid, username, text):
    id = max(mashone.users.keys(), key=mashone.get_karma)
    return '%s (%s)' % (mashone.users[id], id)

def show_worst(mashone, userid, username, text):
    id = min(mashone.users.keys(), key=mashone.get_karma)
    return '%s (%s)' % (mashone.users[id], id)

def debug_show_karma(mashone, userid, username, text):
    return str(mashone.user_karma)

eatfem1 = r'((ем)|(еш)|(жрат)|(жар)|(варит)|(варен)).*((собак)|(жен)|(бывшая)|(бывшую)|(доч)|(сын)|(реб))'
eatfem2 = r'((собак)|(жен)|(бывшая)|(бывшую)|(доч)|(сын)|(реб)).*((ем)|(еш)|(жрат)|(жар)|(варит)|(варен))'

def triggers(botname):
    return [
        Trigger.Simple(r'^нет$', phrases.speak_to_no),
        Trigger.Simple(r'((ненави)|(боюсь)).*((мужик)|(парн))', phrases.speak_me_too, 1, gte(-4)),
        Trigger.Simple(r'((мертв)|(дохл)).*((женщ)|(дев)|(бывш[а][у])|(ваги)|(шлюх)|(пизд[ау]))',
                       phrases.speak_curses, -1),
        Trigger.Simple(r'((трах)|(еба[тнл])|(ебля)|(ебу)).*((мертв)|(труп)|(дет)|(шлюх)|(реб)|(собак))',
                       phrases.speak_curses, -1),
        Trigger.Simple(r'ббпе', phrases.speak_curses, -1),
        Trigger.Simple(r'аборт.*(?!не).*убийство', phrases.speak_curses, -1),
        Trigger.Simple(r'((жен)|(пиз)|(ваг)).*эпил', phrases.speak_curses, -1),
        Trigger.Simple(r'эпил.*((жен)|(пиз)|(ваг))', phrases.speak_curses, -1),
        Trigger.Simple(r'воло.*((пиз)|(ваг))', phrases.speak_curses, -1),
        Trigger.Simple(r'((пиз)|(ваг)).*воло', phrases.speak_curses, -1),
        Trigger.Simple(eatfem1, phrases.speak_curses, -1),
        Trigger.Simple(eatfem2, phrases.speak_curses, -1),
        Trigger.Replacer(r'boy', ['girl']),
        Trigger.Replacer(r'бой', ['гёрл']),
        Trigger.Replacer(r'боя', ['гёрла']),
        Trigger.Replacer(r'бою', ['гёрле']),
        Trigger.Replacer(r'бои', ['гёрлы']),
        Trigger.Simple(r'питер', phrases.speak_piter),
        Trigger.Replacer(r'шаурм', ['шаверм']),
        Trigger.Replacer(r'бордюр', ['поребрик']),
        Trigger.Replacer(r'батон', ['булка']),
        Trigger.Replacer(r'мужик', ['спермобак', 'угнетатель', 'хуемразь']),
        Trigger.Simple(r'%s.*как.*((дела)|(поживаешь))' % botname, phrases.speak_random_question),
        Trigger.Simple(r'за.*сестру.*выпью.*менстру', phrases.speak_compliment, +3),
        Trigger.Simple(r'%s.*(?!не).*(сильная|умная|независимая).*(?!но)' % botname,
                       phrases.speak_compliment, +1),
        Trigger.Command(r'%s.*прости' % botname, userupkarma, k_predicate=gte(5), refusal=phrases.no),
        Trigger.Simple(r'<\$PHOTO>$', phrases.speak_picture),
        Trigger.Command(r'%s.*кто.*лучш(ий)|(ая).*\?' % botname, show_best,
                        k_predicate=gte(-2), refusal=phrases.no),
        Trigger.Command(r'%s.*кто.*худш(ий)|(ая).*\?' % botname, show_worst,
                        k_predicate=gte(-2), refusal=phrases.no),
        Trigger.Command(r'%s.*кто.*хуев(ый)|(ая).*\?' % botname, show_worst,
                        k_predicate=gte(-2), refusal=phrases.no),
        Trigger.Command(r'%s.*debug raise exception' % botname, debug_raise),
        Trigger.Command(r'%s.*debug show karma' % botname, debug_show_karma),
        Trigger.Simple(r'(есть)|(кушать)|(поедать)|(жрать)', phrases.speak_food),
        Trigger.Simple(r'украин', phrases.speak_travel),
        Trigger.Simple(r'порн[оу]', phrases.speak_porn)
    ]