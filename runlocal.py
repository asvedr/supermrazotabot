import masha_setup
import mrazota

alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
alphabet = alphabet + alphabet.upper()

botname = 'Маша'
karma = 'karma.json'

mashone = mrazota.logic.Mashone(alphabet, masha_setup.triggers.triggers(botname), karma)
mashone.REPLACE_TEMPLATE = 'Не {0}, а {1}'
mashone.WEEKDAYS = ["понедельник", "вторник", "среда",
                    "четверг", "пятница", "суббота", "воскресенье"]
mashone.DELTA_KARMA = 3
mashone.NEAREST_ANSWERS = 3
mashone.MIN_KARMA = -10
mashone.MAX_KARMA = 10

userid = '@root'
username = 'root'

while True:
    print(mashone.users)
    print(mashone.user_karma)
    text = input('%s:%s >' % (userid, username))
    print(mashone.read_message(userid, username, text))
