import mrazota
import masha_setup
import sys
import logging

alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
alphabet = alphabet + alphabet.upper()

botname = 'Маша'
karma_file = 'karma.json'

mashone = mrazota.logic.Mashone(alphabet, masha_setup.triggers.triggers(botname), karma_file)
mashone.REPLACE_TEMPLATE = 'Не {0}, а {1}'
mashone.WEEKDAYS = ["понедельник", "вторник", "среда",
                    "четверг", "пятница", "суббота", "воскресенье"]
mashone.DELTA_KARMA = 3
mashone.NEAREST_ANSWERS = 3
mashone.MIN_KARMA = -10
mashone.MAX_KARMA = 10

token = sys.argv[1]

mrazota.telegrambot.main(token, mashone)
